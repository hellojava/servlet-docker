# Install Tomcat & openjdk 8 (openjdk has java and javac)
FROM tomcat:jdk8-openjdk
# Copy source files to tomcat folder structure
COPY . /usr/local/tomcat/webapps/
# -cp, Adding compile time classpath as Tomcat's /lib/servlet-api.jar file.# - d, destination output location.
RUN ["javac", "-cp", ".:/usr/local/tomcat/webapps/lib/javax.servlet-api/3.1.0/javax.servlet-api-3.1.0.jar", "-d", "/usr/local/tomcat/webapps/WEB-INF/classes/", "/usr/local/tomcat/webapps/src/TestingServlet.java"]
#RUN ["javac", "-cp", ".:/usr/local/tomcat/webapps/lib/javax.servlet-api/3.1.0/javax.servlet-api-3.1.0.jar", "-d", "/usr/local/tomcat/webapps/WEB-INF/classes/", "/usr/local/tomcat/webapps/src/ContactFormServlet.java"]
# Serve Tomcat
EXPOSE 8080
CMD ["catalina.sh", "run"]